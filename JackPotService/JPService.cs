﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace JackPotService
{
    public partial class JPService : ServiceBase
    {
        public JPService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            BusinessLogic.StartJackPotWSService();
        }

        protected override void OnStop()
        {
        }
    }
}
