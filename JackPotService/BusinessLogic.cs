﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using WebSocketSharp;
using System.Xml;
using System.IO;
using NLog;
using WebSocketSharp.Server;
using System.Threading;
using System.Timers;
using Newtonsoft.Json;
using System.Configuration;
using Newtonsoft.Json.Linq;

namespace JackPotService
{
    public class JackPot : WebSocketBehavior
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        

        protected override void OnOpen()
        {
            logger.Info("OnOpen");
            logger.Info("New Connection");
            logger.Info("ID " + ID);
            logger.Info("Total number of Session is " + Sessions.Count);
        }
        protected override void OnError(WebSocketSharp.ErrorEventArgs e)
        {
            logger.Error("Onerror");
            logger.Error("Session number " + ID);
            logger.Error("On Error in JackPot " + e.Message);
            logger.Error("On Error in JackPot " + e.Exception);
            logger.Error("Close session");
            Sessions.CloseSession(ID);
            logger.Info("OnError Session count " + Sessions.Count);
        }
        protected override void OnMessage(MessageEventArgs e)
        {
            try
            {
                logger.Info("Message has come");
                logger.Info(e.Data);
                logger.Info("update config file");
                ConfigurationManager.RefreshSection("appSettings");
                var sochi_game_key = ConfigurationManager.AppSettings["sochi_game"];
                logger.Info("sochi game key: " + sochi_game_key);
                JObject js_second = JObject.Parse(e.Data);
                if (js_second.ContainsKey(sochi_game_key))
                {
                    logger.Info("status_code was found");
                    logger.Info("read last data");
                    string last_data = BusinessLogic.readXml();
                    logger.Info("turn to json last data");
                    string last_json_data = BusinessLogic.getJson(last_data);
                    logger.Info("merge two jsons");
                    JObject js_first = JObject.Parse(last_json_data);
                    
                    js_first.Merge(js_second, new JsonMergeSettings
                    {
                        MergeArrayHandling = MergeArrayHandling.Union
                    });

                    string json = js_first.ToString();
                    logger.Info("send broadcast json");
                    logger.Info(json);
                    BusinessLogic.sendDataBroadCast(json);
                }
                else
                {
                    logger.Info("status_code wasn't found");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Ws Client onMessage");
                logger.Error(ex.Message);
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
            }
        }

        protected override void OnClose(CloseEventArgs e)
        {
            logger.Info("OnClose");
            logger.Info("WebSocketServer session {0} has been closed", ID);
            logger.Info(e.Reason);
            logger.Info(e.Code);
            logger.Info("Session count " + Sessions.Count);
        }
    }

    class BusinessLogic
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        static WebSocketServer jackpotBCServer;
        static WebSocket WSClient;
        static int TIMESLEEP = Int32.Parse(
            config.AppSettings.Settings["TimeSleep"].Value);
        static int PORT = Int32.Parse(
            config.AppSettings.Settings["Port"].Value);

        public static void StartJackPotWSService()
        {
            logger.Info("Start Web Socket to download data from  Remote Server");
            
            System.Timers.Timer wsTimer = new System.Timers.Timer(TIMESLEEP);
            wsTimer.Elapsed += new ElapsedEventHandler(checkWebSocket);
            wsTimer.Enabled = true;

            StartWebSocket();
            setWebSocketServer();
            /*
            logger.Info("Start Web Socket Service thread to broadcast data all clients");
            Thread myThread;
            myThread = new Thread(new
            ThreadStart(setWebSocketServer));
            myThread.Start();
            */
        }

        static void setWebSocketServer()
        {
            logger.Info("setWebSocketServer");
            System.Timers.Timer servCheckTimer = new System.Timers.Timer(TIMESLEEP);
            servCheckTimer.Elapsed += new ElapsedEventHandler(checkWebSocket);
            servCheckTimer.Enabled = true;
            logger.Info("startWebSocketServer");
            startWebSocketServer();
        }

        public static void checkServer(object source, ElapsedEventArgs e)
        {
            if (!jackpotBCServer.IsListening)
            {
                try
                {
                    logger.Info("jackpotBCServer restarting!");
                    jackpotBCServer.Stop();
                    startWebSocketServer();
                }
                catch (Exception ex)
                {
                    logger.Error("In checkServer");
                    logger.Error(ex.Message);
                    logger.Error(ex.Data);
                    logger.Error(ex.InnerException);
                    logger.Error(ex.Source);
                }

            }
        }

        
        public static void checkWebSocket(object source, ElapsedEventArgs e)
        {
            if(!WSClient.IsAlive)
            {
                try
                {
                    WSClient.Close();
                    logger.Info("WS restarting!");
                    StartWebSocket();
                }
                catch (Exception ex)
                {
                    logger.Error("checkWebSocket");
                    logger.Error(ex.Message);
                    logger.Error(ex.Data);
                    logger.Error(ex.InnerException);
                    logger.Error(ex.Source);
                }
            }
        }
        
        static void startWebSocketServer()
        {
            PORT = Int32.Parse(
            config.AppSettings.Settings["Port"].Value);

            jackpotBCServer = new WebSocketServer(PORT);
            try
            {
                logger.Info("Websocket Server will be started ");
                jackpotBCServer.AddWebSocketService<JackPot>("/Jackpot", () => new JackPot()
{
        // To ignore the extensions requested from a client.
        IgnoreExtensions = true
} );
                jackpotBCServer.Start();
                logger.Info("Websocket Sever has been started on port  " + PORT);
            }
            catch (Exception ex)
            {
                logger.Error("StartWebSocketServer");
                logger.Error("Exception {0}", ex.ToString());
                logger.Error("Web Socket  Server will be stopped");
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
                jackpotBCServer.Stop();
            }
        }

        public static void OnFinish()
        {
            logger.Trace("On Finish: Sending data is finished");
        }

        public static void StartWebSocket()
        {
           
            {
                Uri siteUri = new Uri(
                    config.AppSettings.Settings["URLToRequest"].Value);
                WSClient = new WebSocket(siteUri.ToString());
                try
                {
                    WSClient.WaitTime = new TimeSpan(0, 0, 60);
                
                    WSClient.OnMessage += (sender, e) =>
                    {
                        try
                        {
                            logger.Trace("On Message Received message");
                            logger.Trace("Data is " + e.Data.ToString());
                            string data = e.Data;

                            logger.Trace("On Message write to xml file");
                            writeToXml(data);
                            
                            logger.Trace("On turn to json data");
                            string jsonData = getJson(data);
                            logger.Trace("pass data to sendDataBroadCast");
                            sendDataBroadCast(jsonData);
                        }
                        catch (Exception ex)
                        {
                            
                            logger.Error("Web Socket Service onMessage");
                            logger.Error("Exception {0}", ex.ToString());
                            logger.Error("Stop server because of error");
                            //temp
                            logger.Error("temprorary turned off");
                            //  jackpotBCServer.Stop();
                        }
                    };
                    WSClient.OnClose += (sender, e) =>
                    {
                        logger.Error("On close in ws");
                        logger.Error("Connection has been closed code: {0} the reason {1}", e.Code, e.Reason);
                        logger.Error("Code {0}", e.Code);
                        logger.Error("Sender " + sender.ToString());
                        //Thread.Sleep(TIMESLEEP);                     
                    };
                    logger.Info("Web socket connects to thirdparty ");
                    WSClient.Connect();
                    if(WSClient.IsAlive)
                    {
                        logger.Info("Web socket connection is established ");
                        string str = getString();
                        logger.Trace("String in ws " + str);
                        WSClient.Send(str);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Start Web Socket");
                    logger.Error(ex.Message);
                    logger.Error(ex.Data);
                    logger.Error(ex.InnerException);
                    logger.Error(ex.Source);
                    logger.Error("WebSocket  will be closed");
                    WSClient.Close();
                }
            }
        }

        public static void sendDataBroadCast(string jsonData)
        {
            try
            {
                lock (jackpotBCServer)
                {
                    if (jackpotBCServer.IsListening)
                    {
                        logger.Trace("sendDataBroadCast method");
                        logger.Trace("Send to all clients");
                        logger.Trace("jsonData " + jsonData);
                        //logger.Trace("Json Data {0}", jsonData);
                        logger.Trace("send broadcast ");
                        jackpotBCServer.WebSocketServices.BroadcastAsync(jsonData, OnFinish);
                    }
                    else
                    {
                        logger.Info("Nobody is listing");
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error("Web Socket Service onMessage");
                logger.Error("Exception {0}", ex.ToString());
                logger.Error("Stop server because of error");
                logger.Error("temprorary turned off");
                // temp
                // jackpotBCServer.Stop();
            }
        }
        /*
        public static void sendData(string data)
        {
            try
            {
                string data1 = data.clo 
                logger.Trace("sendData");
                logger.Trace("write to xml file");
                writeToXml(data);
                if (jackpotBCServer.IsListening)
                {
                    logger.Trace("Send to all clients");
                    string jsonData = getJson(data);
                    logger.Trace("jsonData " + jsonData);
                    //logger.Trace("Json Data {0}", jsonData);
                    logger.Trace("send broadcast ");
                    jackpotBCServer.WebSocketServices.Broadcast(jsonData);
                }
            }
            catch(Exception ex)
            {
                logger.Error("sendData");
                logger.Error(ex.Message);
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
            }

        }
        */
        public static string getString()
        {
            try
            {
                logger.Trace("Get string to send message to thirparty server for establishing conneciton");
                XmlDocument xmlDoc = new XmlDocument();
                string strFile = config.AppSettings.Settings["MessageToRequestPath"].Value;
                logger.Trace("File with request data " + strFile);
                xmlDoc.Load(strFile);
                // Now create StringWriter object to get data from xml document.
                logger.Trace("Create StringWriter");
                StringWriter sw = new StringWriter();
                logger.Trace("Create XmlTextWriter");
                XmlTextWriter xw = new XmlTextWriter(sw);
                xmlDoc.WriteTo(xw);
                String XmlString = sw.ToString();
                logger.Trace("Request string " + XmlString);
                return XmlString;
            }
            catch (Exception ex)
            {
                logger.Error("getString");
                logger.Error(ex.Message);
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
                return "";
            }

        }

        public static string getJson(string xml)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                string json = JsonConvert.SerializeXmlNode(doc);
                return json;
            }
            catch (Exception ex)
            {
                logger.Error("getJson");
                logger.Error(ex.Message);
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
                return JsonConvert.SerializeObject("exception");
            }
        }

        public static void writeToXml(string inputString)
        {
            try
            {
                logger.Trace("Write message to xml file");
               
                string fileName = config.AppSettings.Settings["XMLStorageFilePath"].Value;

                logger.Trace("Data for writing " + inputString);
                logger.Trace("fileName "  + fileName);
                System.IO.File.WriteAllText(fileName, inputString);
            }
            catch (Exception ex)
            {
                logger.Error("write to Xml");
                logger.Error(ex.Message);
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
            }
        }

        public static string readXml()
        {
            try
            {
                logger.Trace("Write message to xml file");

                string fileName = config.AppSettings.Settings["XMLStorageFilePath"].Value;

                logger.Trace("fileName " + fileName);
                string xmlText = System.IO.File.ReadAllText(fileName); 
                return xmlText;
            }
            catch (Exception ex)
            {
                logger.Error("read Xml");
                logger.Error(ex.Message);
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
                return "";
            }
        }

    }
}

// WSClient.Connect();
// ws = new WebSocket("ws://thirdparty.intelligentgaming.net/SignageOpen/JackpotsGateway?COMPUTERNAME=ScalaTest");


// @"G:\Casino\test.xml";
/*
XmlDocument xmlDoc = new XmlDocument();
xmlDoc.LoadXml(inputString);
XmlWriter xmlWriter = XmlWriter.Create(fileName);
xmlWriter.WriteRaw(inputString);         
xmlWriter.Close();
*/
// using (ws = new WebSocket("ws://thirdparty.intelligentgaming.net/SignageOpen/JackpotsGateway?COMPUTERNAME=ScalaTest"))
/*
       public static void StartWebSocket()
       {
           using (var ws = new WebSocket("ws://thirdparty.intelligentgaming.net/SignageOpen/JackpotsGateway?COMPUTERNAME=ScalaTest"))
           // using (var ws = new WebSocket("ws://localhost:4649/Laputa"))
           {
               try
               {
                   // WSClient.OnMessage += (sender, e) =>
                   // logger.Info("Laputa says: " + e.Data);
                   logger.Info("Web socket is starting");
                   WSClient.WaitTime = new TimeSpan(0, 0, 60);

                   WSClient.OnMessage += (sender, e) =>
                    writeToXml(e.Data);

                   WSClient.OnClose += (sender, e) =>
                   {
                       logger.Info("Connection has been closed code: {0} the reason {1}", e.Code, e.Reason);
                       logger.Info("Code {0}", e.Code);
                       System.Threading.Thread.Sleep(15000);
                       StartWebSocket();
                   };

                   logger.Info("Try to connect");
                       WSClient.Connect();
                       logger.Info("Is Alive? {0}", WSClient.IsAlive);
                       string str = getString();
                       WSClient.Send(str);
               }
               catch (Exception ex)
               {
                   logger.Info("Exception {0}", ex.ToString());
                   System.Threading.Thread.Sleep(15000);
                   StartWebSocket();
               }
           }
       }


       public static string getString()
       {
           XmlDocument xmlDoc = new XmlDocument();
           string strFile = @"G:\Casino\message.xml";
           xmlDoc.Load(strFile);

           // Now create StringWriter object to get data from xml document.
           StringWriter sw = new StringWriter();
           XmlTextWriter xw = new XmlTextWriter(sw);
           xmlDoc.WriteTo(xw);
           String XmlString = sw.ToString();
           return XmlString;
       }

       public static void writeToXml(string inputString)
       {
           logger.Info("Server answer: " + inputString);

           XmlDocument xmlDoc = new XmlDocument();

           xmlDoc.LoadXml(inputString);

           string fileName = @"G:\Casino\test" +
                DateTime.Now.Millisecond.ToString() +
                @".xml";
           XmlWriter xmlWriter = XmlWriter.Create(fileName);
           xmlWriter.WriteRaw(inputString);
           xmlWriter.Close();

       }
       */
